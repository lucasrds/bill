﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillDataExtractor.Database
{
    public interface IDatabaseConnection<out T>
    {
        T Connection { get; }
    }
}
