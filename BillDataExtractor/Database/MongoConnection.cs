﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using MongoDB.Driver;

namespace BillDataExtractor.Database
{
    public class MongoConnection : IMongoConnection
    {

        public IMongoClient Connection { get; }
        public MongoConnection()
        {
            var connectionString = @"mongodb://billsapp:OGpWab8Undg3dPDOjHFK0SkfSgEg7lKzLST4rcZ9CcJ1VyMWe672sP5EoKqctmMQ5JudjYPmGQMLK5zVXaI0IA==@billsapp.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
            var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            Connection = new MongoClient(settings);
        }

    }
}