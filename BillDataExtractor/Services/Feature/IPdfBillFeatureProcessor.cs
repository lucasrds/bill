﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Feature
{
    public interface IPdfBillFeatureProcessor : IBillFeatureProcessor<PdfReader, string>
    {
    }
}
