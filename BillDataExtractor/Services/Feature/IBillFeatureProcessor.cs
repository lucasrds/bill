﻿namespace BillDataExtractor.Services.Feature
{
    public interface IBillFeatureProcessor<in T, out TR>
    {
        TR Process(Models.Feature feature, T input);
    }
}
