﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BillDataExtractor.Services.Area;
using BillDataExtractor.Utils;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Feature
{
    public class PdfBillDataFeatureProcessor : IPdfBillFeatureProcessor
    {
        private readonly IPdfTextAreaExtractor _pdfAreaExtractor;
        private readonly IPdfTextAreaFinder _pdfAreaFinder;
        public PdfBillDataFeatureProcessor(IPdfTextAreaExtractor pdfAreaExtractor, IPdfTextAreaFinder pdfAreaFinder)
        {
            _pdfAreaExtractor = pdfAreaExtractor;
            _pdfAreaFinder = pdfAreaFinder;
        }

        public string Process(Models.Feature feature, PdfReader pdf)
        {
            var pageSize = pdf.GetPageSize(feature.Page);
            var boundingBox = feature.BoundingBox;
            var left = boundingBox.Left;
            var right = boundingBox.Right;
            var bottom = boundingBox.Bottom;
            var top = boundingBox.Top;

            if (feature.Relative != null)
            {
                var relative = feature.Relative;
                var points = _pdfAreaFinder.Find(pdf, relative.Text, feature.Page);
                var foundArea = points.First().Rect;

                left = Math.Abs(foundArea.Left + relative.Margin.Left);
                var horizontalSize = boundingBox.Right - boundingBox.Left;
                right = left + horizontalSize;

                top = CoordinateSystem.ToTopDown(pageSize, Math.Abs(foundArea.Top - relative.Margin.Top));
                var verticalSize = boundingBox.Top - boundingBox.Bottom;
                bottom = top + verticalSize;

            }

            bottom = CoordinateSystem.ToTopDown(pageSize, bottom);
            top = CoordinateSystem.ToTopDown(pageSize, top);
            var extractionArea = new Rectangle(left, bottom, right, top);
            var extractedAreaText = _pdfAreaExtractor.Extract(pdf, extractionArea, feature.Page);
            return extractedAreaText;
        }
    }
}