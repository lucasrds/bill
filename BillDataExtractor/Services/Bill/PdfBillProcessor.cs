﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillDataExtractor.Models;
using BillDataExtractor.Services.Bill.Common;
using iTextSharp.text.pdf;
using BillDataExtractor.Services.Text;
using BillDataExtractor.Services.Prediction;
using BillDataExtractor.Utils;

namespace BillDataExtractor.Services.Bill
{
    public class PdfBillProcessor : IPdfBillProcessor
    {
        private readonly IEnumerable<IPdfBillDataExtractor> _datatExtractors;
        private readonly IImagePredictionApi _imagePredictionApi;

        public PdfBillProcessor(IEnumerable<IPdfBillDataExtractor> dataExtractors, IImagePredictionApi imagePredictionApi)
        {
            _imagePredictionApi = imagePredictionApi;
            _datatExtractors = dataExtractors;
        }

        public async Task<string> ClassifyBill(byte[] bill)
        {
            var image = await PdfUtils.PdfBytesToImage(bill);
            var billClass = await _imagePredictionApi.Predict(image);
            return billClass;
        }

        public BillData ExtractData(byte[] bill, string billClass)
        {
            var pdfBill = new PdfReader(bill);
            var dataExtractor = _datatExtractors.ToList()
                .FindAll(extractor => extractor.Type.Equals(billClass))
                .First();

            if (dataExtractor == null)
                throw new NotImplementedException($"Couldnt find any implementation class of type '{billClass}'");

            var billData = dataExtractor.Extract(pdfBill);
            return billData;
        }

        //public T ExtractData<T>(byte[] bill, string billClass, Func<IDictionary<string, string>, T> parse)
        //    where T : BillData
        //{
        //    var pdfBill = new PdfReader(bill);
        //    var data = new Dictionary<string, object> { { "used-class", billClass } };

        //    _textExtractors.ToList()
        //        .FindAll(textExtractor => textExtractor.Type.Equals(billClass))
        //        .ForEach(textExtractor =>
        //        {
        //            var extractedDict = textExtractor.Extract(pdfBill);
        //            data = data.Concat(extractedDict).ToDictionary(k => k.Key, v => v.Value);
        //        });

        //    return parse(data);
        //}
    }
}