﻿using BillDataExtractor.Models;
using System.Threading.Tasks;
using BillDataExtractor.Repository;
using BillDataExtractor.Services.Bill.Common;

namespace BillDataExtractor.Services.Bill
{
    class BillService : IBillService
    {
        private readonly IPdfBillProcessor _pdfProcessor;
        private readonly IBillDataRepository _billDataRepository;
        public BillService(IPdfBillProcessor pdfProcessor, IBillDataRepository billDataRepository)
        {
            _pdfProcessor = pdfProcessor;
            _billDataRepository = billDataRepository;
        }

        public async Task<BillData> GetBillData(byte[] bill)
        {
            var billClass = await _pdfProcessor.ClassifyBill(bill);
            return _pdfProcessor.ExtractData(bill, billClass);
        }

        public Task SaveBillData(BillData data) => _billDataRepository.Save(data);
    }
}
