﻿using System.Threading.Tasks;
using BillDataExtractor.Models;

namespace BillDataExtractor.Services.Bill.Common
{
    public interface IBillService
    {
        Task<BillData> GetBillData(byte[] bill);
        Task SaveBillData(BillData data);

    }
}
