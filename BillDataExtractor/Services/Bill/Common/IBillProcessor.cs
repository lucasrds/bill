﻿using BillDataExtractor.Models;
using System.Collections.Generic;

namespace BillDataExtractor.Services.Bill
{
    interface IBillProcessor<T, TR> : IBillClassifier<T>, IBillExtractor<TR> where TR : BillData
    {
        
    }
}
