﻿using System.Threading.Tasks;

namespace BillDataExtractor.Services.Bill
{
    interface IBillClassifier<T>
    {
        Task<T> ClassifyBill(byte[] bill);
    }
}
