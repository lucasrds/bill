﻿namespace BillDataExtractor.Services.Bill
{
    interface IBillExtractor<T>
    {
        T ExtractData(byte[] bill, string billClass);
    }
}
