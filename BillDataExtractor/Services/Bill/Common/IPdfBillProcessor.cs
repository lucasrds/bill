﻿using System.Collections.Generic;
using BillDataExtractor.Models;

namespace BillDataExtractor.Services.Bill.Common
{
    interface IPdfBillProcessor : IBillProcessor<string, BillData>
    {
    }
}
