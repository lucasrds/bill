﻿using System.Collections.Generic;
using BillDataExtractor.Models;
using BillDataExtractor.Services.Feature;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Text
{
    public abstract class MappedPdfBillDataExtractor : IPdfBillDataExtractor
    {
        public abstract string Type { get; }
        private readonly IPdfBillFeatureProcessor _pdfBillFeatureProcessor;
        protected MappedPdfBillDataExtractor(IPdfBillFeatureProcessor pdfBillFeatureProcessor)
        {
            _pdfBillFeatureProcessor = pdfBillFeatureProcessor;
        }
        
        public abstract Mapping GetMappedFeatures();

        public abstract BillData ConvertExtractedToBillData(IDictionary<string, object> data);

        public Dictionary<string, object> ProcessFeatures(PdfReader pdfReader, Mapping mapping)
        {
            var dataDictionary = new Dictionary<string, object>();
            foreach (var feature in mapping.Features)
            {
                var extractedAreaText = _pdfBillFeatureProcessor.Process(feature, pdfReader);
                dataDictionary.Add(feature.Name, extractedAreaText);
            }
            return dataDictionary;
        }

        public BillData Extract(PdfReader pdfReader)
        {
            var features = GetMappedFeatures();
            var dataDictionary = ProcessFeatures(pdfReader, features);
            var billData = ConvertExtractedToBillData(dataDictionary);
            billData.UsedClass = Type;
            return billData;
        }
    }
}
