﻿using iTextSharp.text.pdf;
using System.Collections.Generic;

namespace BillDataExtractor.Services.Text
{
    public interface IPdfDataExtractor<out T>
    {
        string Type { get; }
        T Extract(PdfReader pdfReader);
    }
}
