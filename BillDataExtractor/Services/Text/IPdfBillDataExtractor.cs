﻿using BillDataExtractor.Models;

namespace BillDataExtractor.Services.Text
{
    public interface IPdfBillDataExtractor : IPdfDataExtractor<BillData>
    {
    }
}
