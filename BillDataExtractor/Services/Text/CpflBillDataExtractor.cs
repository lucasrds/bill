﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BillDataExtractor.Services.Area;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Text
{
    class CpflBillDataExtractor : MappedCsvBillTextExtractor
    {
        public override string Type => "cpfl";
        public CpflBillDataExtractor(IPdfTextAreaExtractor pdfAreaExtractor) : base(pdfAreaExtractor)
        {
        }

        public override List<string> GetMappedFeatures()
        {
            return Regex.Split(Properties.Resources.CpflFaturaMapping, @"\r\n").ToList();
        }
    }
}
