﻿using System.Collections.Generic;
using System.Linq;
using BillDataExtractor.Services.Area;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Text
{
    public abstract class MappedCsvBillTextExtractor : IPdfDataExtractor<Dictionary<string, object>>
    {
        private readonly IPdfTextAreaExtractor _pdfAreaExtractor;

        public abstract string Type { get; }

        public abstract List<string> GetMappedFeatures();

        protected MappedCsvBillTextExtractor(IPdfTextAreaExtractor pdfAreaExtractor)
        {
            _pdfAreaExtractor = pdfAreaExtractor;
        }

        public Dictionary<string, object> Extract(PdfReader pdfReader)
        {
            var features = GetMappedFeatures();
            var dataDictionary = ProcessFeatures(pdfReader, features);
            return dataDictionary;
        }

        public string ExtractPerFeature(PdfReader pdfReader, string feature)
        {
            var pageSize = pdfReader.GetPageSize(1).Height;
            var splitedLine = feature.Split(',');
            var x1 = float.Parse(splitedLine.ElementAt(1));
            var x2 = float.Parse(splitedLine.ElementAt(2));
            var y1 = pageSize - float.Parse(splitedLine.ElementAt(3));
            var y2 = pageSize - float.Parse(splitedLine.ElementAt(4));
            var extractedAreaText = _pdfAreaExtractor.Extract(pdfReader, new Rectangle(x1, y1, x2, y2));
            return extractedAreaText;
        }
        public Dictionary<string, object> ProcessFeatures(PdfReader pdfReader, List<string> features)
        {
            var dataDictionary = new Dictionary<string, object>();
            features.RemoveAt(0);
            features.ForEach(feature =>
            {
                var featureName = feature.Split(',').ElementAt(0);
                var extractedAreaText = ExtractPerFeature(pdfReader, feature);
                dataDictionary.Add(featureName, extractedAreaText);
            });
            return dataDictionary;
        }

    }
}
