﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using BillDataExtractor.Models;
using BillDataExtractor.Repository;
using BillDataExtractor.Services.Feature;
using BillDataExtractor.Utils;
using BillDataExtractor.Extensions;

namespace BillDataExtractor.Services.Text
{
    class AesBillDataExtractor : MappedPdfBillDataExtractor
    {
        public override string Type => "aes";
        private readonly IMappingRepository _mappingRepository;
        public AesBillDataExtractor(IPdfBillFeatureProcessor pdfBillFeatureProcessor, IMappingRepository mappingRepository) : base(pdfBillFeatureProcessor)
        {
            _mappingRepository = mappingRepository;
        }

        public override Mapping GetMappedFeatures() => _mappingRepository.Find(mapping => mapping.Category.Contains("aes")).First();
        public override BillData ConvertExtractedToBillData(IDictionary<string, object> data)
        {
            var billData = BillDataUtils.DictToBillData(data);
            //var rawData = billData.Raw;
            //var client = new Client
            //{
            //    Name = (string)rawData["NomeCliente"],
            //    Cnpj = Regex.Split((string)rawData["DadosCliente"], "\n").ToList().Find(s => s.Contains("CNPJ"))
            //};
            //billData.Client = client;

            //var dateFormat = "dd.MM.yyyy";
            //var dateString = Regex.Split((string)rawData["LeituraAtual"], "Atual ")[1];
            //billData.ActualRead = DateTime.Now.StringToDateTime(dateString, dateFormat);

            //billData.Installation = (string) rawData["NumeroDoMedidor"];
            //billData.Equipament = (string) rawData["NumeroDoMedidor"];
            //billData.TaxType = (string) rawData["CategoriaTarifa"];

            //var valuesBlock = Regex.Split((string) rawData["FullValuesBlock"], "\n");
            
            //billData.Cofins =
            //    float.Parse(valuesBlock.ToList().Find(s => s.Contains("0699 COFINS")).Split(' ')[6].Replace(",", "."));

            //billData.PisPasep =
            //    float.Parse(valuesBlock.ToList().Find(s => s.Contains("PIS/PASEP")).Split(' ')[6].Replace(",", "."));

            //billData.Cosip = new Cosip
            //{
            //    Price = float.Parse(valuesBlock.ToList().Find(s => s.Contains("COSIP")).Split(' ')[4].Replace(",","."))
            //};

            //var demands = new List<Demand>();
            //demands.Add(new Demand
            //{
            //    Type = "demandaIndutiva",
            //    Ponta = false,
            //    Registered = float.Parse(valuesBlock.ToList().Find(s => s.Contains("DEMANDA FORA PONTA INDUTIVA")).Split(' ')[6].Replace(",", "."))
            //});
            //demands.Add(new Demand
            //{
            //    Type = "demandaCapacitativa",
            //    Ponta = false,
            //    Registered = float.Parse(valuesBlock.ToList().Find(s => s.Contains("DEMANDA FORA PONTA CAPACITIVA")).Split(' ')[6].Replace(",", "."))
            //});
            //demands.Add(new Demand
            //{
            //    Type = "demandaPonta",
            //    Ponta = true,
            //    Registered = float.Parse(valuesBlock.ToList().Find(s => s.Contains("DEMANDA PONTA")).Split(' ')[4].Replace(",", "."))
            //});
            //billData.Demands = demands;

            return billData;
        }
        
    }
}
