﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Area
{
    public interface IPdfTextAreaExtractor
    {
        string Extract(PdfReader reader, Rectangle extractionArea, int page = 1);
    }
}
