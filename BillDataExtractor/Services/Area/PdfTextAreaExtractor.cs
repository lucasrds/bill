﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;

namespace BillDataExtractor.Services.Area
{
    class PdfTextAreaExtractor : IPdfTextAreaExtractor
    {
        public string Extract(PdfReader reader, Rectangle extractionArea, int page = 1)
        {
            RenderFilter[] renderFilter = new RenderFilter[1];
            renderFilter[0] = new RegionTextRenderFilter(extractionArea);
            ITextExtractionStrategy textExtractionStrategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), renderFilter);
            return PdfTextExtractor.GetTextFromPage(reader, page, textExtractionStrategy);
        }
    }
}
