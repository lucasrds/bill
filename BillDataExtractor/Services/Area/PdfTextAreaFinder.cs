﻿using System.Collections.Generic;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace BillDataExtractor.Services.Area
{
    public class PdfTextAreaFinder : IPdfTextAreaFinder
    {
        public List<RectText> Find(PdfReader pdf, string text, int page)
        {
            var strategy = new TextLocationFinderStrategy(text);
            PdfTextExtractor.GetTextFromPage(pdf, page, strategy);
            return strategy.Points;
        }
    }
}