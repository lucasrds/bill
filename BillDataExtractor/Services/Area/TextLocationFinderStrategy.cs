﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;

namespace BillDataExtractor.Services.Area
{
    public class TextLocationFinderStrategy : LocationTextExtractionStrategy
    {
        public List<RectText> Points { get; } = new List<RectText>();
        private readonly string _textToSearchFor;
        private readonly CompareOptions _compareStringOptions;

        public TextLocationFinderStrategy(string textToSearchFor, CompareOptions compareStringOptions = CompareOptions.None)
        {
            _textToSearchFor = textToSearchFor;
            _compareStringOptions = compareStringOptions;
        }

        //Automatically called for each chunk of text in the PDF
        public override void RenderText(TextRenderInfo renderInfo)
        {
            base.RenderText(renderInfo);
            //See if the current chunk contains the text
            var startPosition = CultureInfo.CurrentCulture.CompareInfo.IndexOf(renderInfo.GetText(), _textToSearchFor, _compareStringOptions);
            //If not found bail
            if (startPosition < 0)
                return;
            //Grab the individual characters
            var chars = renderInfo.GetCharacterRenderInfos().Skip(startPosition).Take(_textToSearchFor.Length).ToList();
            //Grab the first and last character
            var firstChar = chars.First();
            var lastChar = chars.Last();
            //Get the bounding box for the chunk of text
            var bottomLeft = firstChar.GetDescentLine().GetStartPoint();
            var topRight = lastChar.GetAscentLine().GetEndPoint();
            //Create a rectangle from it
            var rect = new Rectangle(bottomLeft[Vector.I1], bottomLeft[Vector.I2], topRight[Vector.I1], topRight[Vector.I2]);
            //Add this to our main collection
            Points.Add(new RectText(rect, _textToSearchFor));
        }
    }
}
