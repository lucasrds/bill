﻿using System.Collections.Generic;
using iTextSharp.text.pdf;

namespace BillDataExtractor.Services.Area
{
    public interface IPdfTextAreaFinder
    {
        List<RectText> Find(PdfReader pdf, string text, int page = 1);
    }
}
