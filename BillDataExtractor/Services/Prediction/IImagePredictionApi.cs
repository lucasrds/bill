﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillDataExtractor.Services.Prediction
{
    public interface IImagePredictionApi : IPredictionApi<Task<string>, byte[]>
    {
    }
}
