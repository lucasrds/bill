﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BillDataExtractor.Extensions;
using BillDataExtractor.Utils;

namespace BillDataExtractor.Services.Prediction
{
    public class CustomVisionPredictionApi : IImagePredictionApi
    {

        public async Task<string> Predict(byte[] input)
        {
            var content = input.ConvertBytesToByteArrayContent();
            content.Headers.Add("Content-Type", "application/octet-stream");
            content.Headers.Add("Prediction-Key", Properties.Resources.PredictionKey);
            using (var httpClient = new HttpClient())
                return await httpClient
                    .PostAsync(Properties.Resources.CustomVisionClassificationApiUrl, content)
                    .ReadAsStringAsync()
                    .ContinueWith(GetTagName);
        }
        public string GetTagName(Task<string> task)
        {
            return AiModelUtil
                .StringToAiModel(task.Result)
                .Predictions
                .First().TagName;
        }
    }
}