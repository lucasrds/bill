﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BillDataExtractor.Models;
using Newtonsoft.Json;

namespace BillDataExtractor.Utils
{
    public class AiModelUtil
    {

        public static AiModel StringToAiModel(string modelString) => JsonConvert.DeserializeObject<AiModel>(modelString);
        public static string AiModelToJson(AiModel model) => JsonConvert.SerializeObject(model, Formatting.Indented);
    }
}