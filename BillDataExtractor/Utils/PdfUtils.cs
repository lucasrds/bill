﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using BillDataExtractor.Extensions;

namespace BillDataExtractor.Utils
{
    public class PdfUtils
    {
        public static async Task<byte[]> PdfBytesToImage(byte[] bytes)
        {
            var exe = Paths.PngConverterPath;
            var mediaPath = Paths.MediaPath;

            var fileName = Guid.NewGuid();
            var inputFile = $"{mediaPath}/{fileName}.pdf";
            var outputPath = $"{mediaPath}/{fileName}";

            bytes.WriteToFile(inputFile);

            var process = Process.Start(new ProcessStartInfo(exe, $" \"{inputFile}\" \"{outputPath}\"")
            {
                CreateNoWindow = true,
                ErrorDialog = false,
                WindowStyle = ProcessWindowStyle.Hidden
            });

            await process.ExitedAsync();

            var image = File.ReadAllBytes($"{outputPath}-000001.png");

            var mediaFiles = new DirectoryInfo(mediaPath).GetFiles();
            foreach (var file in mediaFiles)
            {
                if(file.Name.Contains(fileName.ToString()))
                    file.Delete();
            }

            return image;

        }
    }
}