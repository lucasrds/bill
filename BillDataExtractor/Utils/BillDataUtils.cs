﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BillDataExtractor.Extensions;
using BillDataExtractor.Models;

namespace BillDataExtractor.Utils
{
    public static class BillDataUtils
    {
        public static BillData DictToBillData(IDictionary<string, object> dictionary)
        {
            var billData = dictionary.ToObject<BillData>();
            var propsNames = typeof(BillData).GetProperties().ToList().Select(prop => prop.Name);
            dictionary.ToList()
                .FindAll(item => !propsNames.Contains(item.Key))
                .ForEach(item => billData.Raw.Add(item.Key, item.Value));    
            return billData;
        } 
    }
}