﻿using System.Configuration;
using System.Web;

namespace BillDataExtractor.Utils
{
    public static class Paths
    {
        public static string MediaPath => $"{HttpRuntime.AppDomainAppPath}{ConfigurationManager.AppSettings["MediaPath"]}";
        public static string PngConverterPath => $"{HttpRuntime.AppDomainAppPath}{ConfigurationManager.AppSettings["PngConverterPath"]}";
    }
}