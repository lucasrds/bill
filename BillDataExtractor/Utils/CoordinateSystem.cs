﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;

namespace BillDataExtractor.Utils
{
    public static class CoordinateSystem
    {
        public static float ToTopDown(Rectangle rect, float y) => Math.Abs(rect.Top - y);
    }
}