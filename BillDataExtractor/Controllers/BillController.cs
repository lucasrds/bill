﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web;
using BillDataExtractor.Services.Bill;
using BillDataExtractor.Extensions;
using BillDataExtractor.Models;
using BillDataExtractor.Repository;
using BillDataExtractor.Services.Bill.Common;
using log4net;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace BillDataExtractor.Controllers
{
    [RoutePrefix("api/v1/bill")]
    public class BillController : ApiController
    {
        private readonly IBillService _billService;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BillController));
        private readonly IMappingRepository _repository;
        private readonly IBillDataRepository _billDataRepo;
        public BillController(IBillService billService, IMappingRepository repository, IBillDataRepository billDataRepo)
        {
            _billService = billService;
            _repository = repository;
            _billDataRepo = billDataRepo;
        }

        [Route("mappings")]
        [HttpGet]
        public HttpResponseMessage GetMappings()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repository.FindAll());
        }

        [Route("mappings")]
        [HttpPost]
        public async Task<HttpResponseMessage> SaveMappings([FromBody] Mapping mapping)
        {
            await _repository.Save(mapping);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("")]
        [HttpPost]
        public async Task<HttpResponseMessage> SaveBillData()
        {
            var data = new BillData();
            data.Client = new Client();
            data.Client.Name = "Lucas";
            data.Raw = new Dictionary<string, object>();
            data.Raw.Add("Teste", "xablau");
            await _billDataRepo.Save(data);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("extract/raw")]
        [HttpPost]
        public async Task<HttpResponseMessage> ExtractRaw()
        {
            //var fileBytes = HttpContext.Current.Request.Files.First().ToBytes();
            var fileBytes = await Request.Content.ReadAsByteArrayAsync();
            return await Extract(fileBytes);
        }


        [Route("extract/base64")]
        [HttpPost]
        public async Task<HttpResponseMessage> ExtractBase64()
        {
            var base64String = await Request.Content.ReadAsStringAsync();
            var fileBytes = Convert.FromBase64String(base64String);
            return await Extract(fileBytes);
        }

        public async Task<HttpResponseMessage> Extract(byte[] data)
        {
            try
            {
                var billData = await _billService.GetBillData(data);
                await _billService.SaveBillData(billData);
                return Request.CreateResponse(HttpStatusCode.OK, billData);
            }
            catch (Exception e)
            {
                Logger.Error("Error in extract ", e);
                throw;
            }
        }


    }
}
