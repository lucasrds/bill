﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace BillDataExtractor.Repository
{
    public interface IMongoRepository<T>
    {
        IQueryable<T> FindAll();
        IQueryable<T> Find(Expression<Func<T, bool>> query);

        Task Save(T type);
        Task Delete(Expression<Func<T, bool>> where);
    }
}
