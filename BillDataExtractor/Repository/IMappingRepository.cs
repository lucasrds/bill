﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BillDataExtractor.Models;

namespace BillDataExtractor.Repository
{
    public interface IMappingRepository : IMongoRepository<Mapping>
    {
    }
}