﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BillDataExtractor.Database;
using BillDataExtractor.Models;
using MongoDB.Driver;

namespace BillDataExtractor.Repository
{
    public class MappingRepository : IMappingRepository
    {
        private readonly IMongoDatabase _db;
        private const string CollectionName = "Mappings";
        private const string DatabaseName = "EdpMappings";

        public MappingRepository(IMongoConnection mongoConnection)
        {
            _db = mongoConnection.Connection.GetDatabase(DatabaseName);
        }
        private IMongoCollection<Mapping> GetCollection()
        {
            return _db.GetCollection<Mapping>(CollectionName);
        }
        public IQueryable<Mapping> FindAll()
        {
            return GetCollection().AsQueryable();
        }

        public IQueryable<Mapping> Find(Expression<Func<Mapping, bool>> query)
        {
            return FindAll().Where(query);
        }

        public Task Save(Mapping type)
        {
            return GetCollection().InsertOneAsync(type);
        }

        public Task Delete(Expression<Func<Mapping, bool>> where)
        {
            return GetCollection().DeleteManyAsync(where);
        }
    }
}