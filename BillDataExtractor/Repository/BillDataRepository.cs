﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using BillDataExtractor.Database;
using BillDataExtractor.Models;
using MongoDB.Driver;

namespace BillDataExtractor.Repository
{
    public class BillDataRepository : IBillDataRepository
    {
        private readonly IMongoDatabase _db;
        private const string CollectionName = "BillData";
        private const string DatabaseName = "EdpMappings";

        public BillDataRepository(IMongoConnection mongoConnection)
        {
            _db = mongoConnection.Connection.GetDatabase(DatabaseName);
        }
        public IQueryable<BillData> FindAll()
        {
            return _db.GetCollection<BillData>(CollectionName).AsQueryable();
        }

        public IQueryable<BillData> Find(Expression<Func<BillData, bool>> query)
        {
            return FindAll().Where(query);
        }

        public Task Save(BillData type)
        {
            return _db.GetCollection<BillData>(CollectionName).InsertOneAsync(type);
        }

        public Task Delete(Expression<Func<BillData, bool>> @where)
        {
            throw new NotImplementedException();
        }
    }
}