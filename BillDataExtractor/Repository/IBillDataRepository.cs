﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillDataExtractor.Models;

namespace BillDataExtractor.Repository
{
    public interface IBillDataRepository : IMongoRepository<BillData>
    {
    }
}
