﻿using System.IO;
using BillDataExtractor.Controllers;
using BillDataExtractor.Services.Bill;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using BillDataExtractor.Database;
using BillDataExtractor.IoC;
using BillDataExtractor.Models;
using BillDataExtractor.Repository;
using BillDataExtractor.Services.Area;
using BillDataExtractor.Services.Bill.Common;
using BillDataExtractor.Services.Feature;
using BillDataExtractor.Services.Prediction;
using BillDataExtractor.Services.Text;
using BillDataExtractor.Utils;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(BillDataExtractor.Startup))]
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace BillDataExtractor
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            ResolveSwagger(config);
            ResolveSerialization(config);
            ResolveLogger(config);
            ResolveDependencies(config);
            ResolveWebApi(app, config);
            ResolveDirectories();
        }

        private static void ResolveSwagger(HttpConfiguration config)
        {
            config.EnableSwagger(c => c.SingleApiVersion("v1", "First Version of app")).EnableSwaggerUi();
        }
        private static void ResolveLogger(HttpConfiguration config)
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private static void ResolveWebApi(IAppBuilder app, HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            app.UseWebApi(config);
        }

        private static void ResolveDependencies(HttpConfiguration config)
        {
            var services = new ServiceCollection();

            services.AddScoped<BillController>();
            services.AddScoped<IMongoConnection, MongoConnection>();
            services.AddScoped<IMappingRepository, MappingRepository>();
            services.AddScoped<IBillDataRepository, BillDataRepository>();
            services.AddScoped<IImagePredictionApi, CustomVisionPredictionApi>();
            services.AddScoped<IPdfBillProcessor, PdfBillProcessor>();
            services.AddScoped<IPdfBillFeatureProcessor, PdfBillDataFeatureProcessor>();
            services.AddScoped<IBillService, BillService>();
            services.AddScoped<IPdfBillDataExtractor, AesBillDataExtractor>();
            services.AddScoped<IPdfTextAreaFinder, PdfTextAreaFinder>();
            services.AddScoped<IPdfTextAreaExtractor, PdfTextAreaExtractor>();
            
            config.DependencyResolver = new DefaultDependencyResolver(services.BuildServiceProvider().CreateScope());
        }

        private static void ResolveSerialization(HttpConfiguration config)
        {
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
        }

        private static void ResolveDirectories()
        {
            if (!Directory.Exists(Paths.MediaPath))
                Directory.CreateDirectory(Paths.MediaPath);
        }

    }
}
