﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Microsoft.Extensions.DependencyInjection;

namespace BillDataExtractor.IoC
{
    public class DefaultDependencyResolver : IDependencyResolver
    {
        private IServiceProvider _serviceProvider;

        public DefaultDependencyResolver(IServiceScope serviceScope)
        {
            _serviceProvider = serviceScope.ServiceProvider;
        }

        public IDependencyScope BeginScope() => new DefaultDependencyResolver(_serviceProvider.CreateScope());

        public void Dispose() => _serviceProvider = null;

        public object GetService(Type serviceType) => _serviceProvider.GetService(serviceType);

        public IEnumerable<object> GetServices(Type serviceType) => _serviceProvider.GetServices(serviceType);
    }
}