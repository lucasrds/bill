﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BillDataExtractor.Extensions
{
    public static class HttpContextExtensions
    {
        public static HttpPostedFile First(this HttpFileCollection httpFileCollection) => httpFileCollection[0];
        public static byte[] ToBytes(this HttpPostedFile file)
        {
            using (var binaryReader = new BinaryReader(file.InputStream))
                return binaryReader.ReadBytes(file.ContentLength);
        }
    }
}