﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BillDataExtractor.Extensions
{
    public static class Extensions
    {
        public static byte[] FromFilePathReadAllBytes(this string filepath) => File.ReadAllBytes(filepath);

        public static void WriteToFile(this byte[] bytes, string path)
        {
            File.Create(path).Close();
            File.WriteAllBytes(path, bytes);
        }

        public static ByteArrayContent ConvertBytesToByteArrayContent(this byte[] bytes) => new ByteArrayContent(bytes);

        public static async Task<string> ReadAsStringAsync(this Task<HttpResponseMessage> response)
        {
            var message = await response;
            return await message.Content.ReadAsStringAsync();
        }

        public static void WriteDictionaryToFile(this Dictionary<string, string> dictionary, string pathToSave)
        {
            var json = JsonConvert.SerializeObject(dictionary, Formatting.Indented);
            File.Create(pathToSave).Close();
            File.WriteAllText(pathToSave, json);
        }

        public static void PrintDictionaryIntoScreen(this Dictionary<string, string> dictionary) =>
            dictionary.ToList().ForEach(keyValue => Console.WriteLine($@"{keyValue.Key} -> {keyValue.Value}\n"));

        public static T ToObject<T>(this IDictionary<string, object> source)
            where T : class, new()
        {
            var someObject = new T();
            var someObjectType = someObject.GetType();

            foreach (var item in source)
            {
                someObjectType
                    .GetProperty(item.Key)?
                    .SetValue(someObject, item.Value, null);
            }

            return someObject;
        }

        public static DateTime StringToDateTime(this DateTime dateTime, string input, string format)
        {
            DateTime.TryParseExact(input,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.AllowWhiteSpaces,
                out var date);
            return date;
        }
    }
}
