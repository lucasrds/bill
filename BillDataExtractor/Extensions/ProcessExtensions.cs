﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace BillDataExtractor.Extensions
{
    public static class ProcessExtensions
    {
        public static Task ExitedAsync(this Process process)
        {
            return Task.Factory.StartNew(process.WaitForExit);
        }
    }
}