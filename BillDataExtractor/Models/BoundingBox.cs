﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    public class BoundingBox
    {
        [BsonElement("left")]
        public float Left { get; set; }
        [BsonElement("right")]
        public float Right { get; set; }
        [BsonElement("bottom")]
        public float Bottom { get; set; }
        [BsonElement("top")]
        public float Top { get; set; }
    }
}