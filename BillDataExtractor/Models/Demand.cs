﻿namespace BillDataExtractor.Models
{
    public class Demand
    {
        public string Type { get; set; } // demanda | demandaLidaKW | demandaTusdKW | demandaContratada | demandaIndutiva | demandaMaxima | demandaMaximaCap | demandaMaximaInd
        public bool Unique { get; set; }
        public bool Ponta { get; set; }
        public string Flag { get; set; } // vermelha | amarela | verde
        public float Registered { get; set; }
        public float Tax { get; set; }
        public float Icms { get; set; }
        public float Hptm { get; set; }
        public float Htfm { get; set; }
        public float Hptf { get; set; }
        public float Htfc { get; set; }
        public float Value { get; set; }

    }
}