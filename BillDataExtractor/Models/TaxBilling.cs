﻿namespace BillDataExtractor.Models
{
    public class TaxBilling
    {
        public string Category { get; set; } // Tusd, Te, Reativa
        public string Type { get; set; } // Individual | Capacidade
        public string Flag { get; set; } // vermelha | amarela | verde
        public bool Ponta { get; set; }
        public float Quantity { get; set; }
        public float UnitPrice { get; set; }
        public float Price { get; set; }
        public float Billed { get; set; }
        public float Provided { get; set; }
        public float Tribute { get; set; }
    }
}