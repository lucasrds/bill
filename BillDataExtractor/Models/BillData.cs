﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    public class BillData
    {
        
        [BsonId]
        public ObjectId _Id { get; set; }
        public DateTime Created { get; } = DateTime.Now;
        public Client Client { get; set; }
        public DateTime ActualRead { get; set; }
        public string Installation { get; set; }
        public string Equipament { get; set; }
        public string TaxType { get; set; }
        public string VoltageSubgroup { get; set; } // Subgrupo de Tensao
        public string ConsumeCategory { get; set; }
        public float OtherCosts { get; set; }
        public float Icms { get; set; }
        public float PisPasep { get; set; }
        public float Cofins { get; set; }
        public FreeMarketTax FreeMarketTax { get; set; }
        public Cosip Cosip { get; set; }
        public IEnumerable<Demand> Demands { get; set; }
        public IEnumerable<TaxBilling> TaxBillings{ get; set; }
        public string UsedClass { get; set; }
        public IDictionary<string, object> Raw { get; set; } = new Dictionary<string, object>();
    }
}