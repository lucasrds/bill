﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;

namespace BillDataExtractor.Services.Area
{
    public class RectText
    {

        public  Rectangle Rect { get; }
        public  string Text { get; }

        public RectText(Rectangle rect, string text)
        {
            Rect = rect;
            Text = text;
        }
}
}