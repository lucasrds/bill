﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    [BsonIgnoreExtraElements]
    public class Mapping
    {
        [BsonId]
        public ObjectId _Id { get; set; }
        [BsonElement("category")]
        public string Category { get; set; }
        [BsonElement("features")]
        public IEnumerable<Feature> Features{ get; set; }
    }
}