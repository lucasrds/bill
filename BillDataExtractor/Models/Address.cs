﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BillDataExtractor.Models
{
    public class Address
    {
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public string City { get; set; }
    }
}