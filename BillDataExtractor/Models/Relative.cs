﻿using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    public class Relative
    {
        [BsonElement("text")]
        public string Text { get; set; }
        [BsonElement("margin")]
        public Margin Margin { get; set; }
    }
}