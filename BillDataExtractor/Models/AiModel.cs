﻿using System;
using System.Collections.Generic;

namespace BillDataExtractor.Models
{
    public class AiModel
    {
        public string Id { get; set; }
        public string Project { get; set; }
        public string Iteration { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Prediction> Predictions { get; set; }
    }
}
