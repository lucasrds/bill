﻿namespace BillDataExtractor.Models
{
    public class FreeMarketTax
    {
        public float Mwh { get; set; }
        public float MwhCash { get; set; }
    }
}