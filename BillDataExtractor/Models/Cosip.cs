﻿namespace BillDataExtractor.Models
{
    public class Cosip
    {
        public float Quantity { get; set; }
        public float Price { get; set; }
    }
}