﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    public class Feature
    {
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("page")]
        public int Page { get; set; } = 1;
        [BsonElement("boundingBox")]
        public BoundingBox BoundingBox { get; set; }
        [BsonElement("relative")]
        public Relative Relative { get; set; }
    }
}