﻿using MongoDB.Bson.Serialization.Attributes;

namespace BillDataExtractor.Models
{
    public class Margin
    {
        [BsonElement("left")]
        public float Left { get; set; }
        [BsonElement("top")]
        public float Top { get; set; }
    }
}