﻿namespace BillDataExtractor.Models
{
    public class Prediction
    {
        public float Probability { get; set; }
        public string TagId { get; set; }
        public string TagName { get; set; }
    }
}
