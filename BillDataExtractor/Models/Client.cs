﻿namespace BillDataExtractor.Models
{
    public class Client
    {
        public string Name { get; set; }
        public string Cnpj { get; set; }
    }
}